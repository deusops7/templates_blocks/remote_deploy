# Variables

```yaml
  NAMEIMAGE:        docker.io/${USERNAME}/${CI_PROJECT_NAME}:latest
  PROJECTNAME:      ${CI_PROJECT_NAME}_${CI_COMMIT_BRANCH}
  IMAGE_DOCKER:     "docker:25.0.3"
  IMAGE_DIND:       "docker:25.0.3-dind"
  HOST_PORT:        "8000"
  CONTAINER_PORT:   "8000"
  PATH_TO_ENV_FILE: "${CI_PROJECT_DIR}/env-file.env"
  HOST_DIR:         "/data"
  CONTAINER_DIR:    "/data"
```
